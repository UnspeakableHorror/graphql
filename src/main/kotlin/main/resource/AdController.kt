package main.resource

import adgraph.AdRepository
import adgraph.QL
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.stream.Collectors

/**
 * @author uh on 2018/07/02.
 */
@RestController
class AdController {

    val schema = QL(AdRepository())

    @RequestMapping(value = ["/ad"], produces = ["application/json"])
    fun getAd(@RequestParam id: Long): String? {
        return schema.execute("{ad(id: $id){ title categories{name} ... on Ad{} } }")
    }

    @RequestMapping(value = ["/ad-fields"], produces = ["application/json"])
    fun getAdFields(@RequestParam id: Long, @RequestParam fields: List<String>): String? {
        return schema.execute("{ad(id: $id){ ${fields.joinToString(" ")} ... on Ad{} } }")
    }

    @RequestMapping(value = ["/ads"], produces = ["application/json"])
    fun getAds(@RequestParam ids: List<Long>): String? {
        return schema.execute("{ad(id: $ids){ title categories ... on Ad{} } }")
    }
}
