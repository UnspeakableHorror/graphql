package main.resource

import graph.schema
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * @author uh on 2018/07/02.
 */
@RestController
class Graph {

    @RequestMapping(value = ["/heroes"], produces = ["application/json"])
    fun heroes(): String {
        return schema.execute("{heroes { id name ... on Droid{primaryFunction} ... on Human{height} } }")
    }

    @RequestMapping(value = ["/hero"], produces = ["application/json"])
    fun hero(): String {
        return schema.execute("{hero(episode: JEDI){ id name ... on Droid{primaryFunction} ... on Human{height} } }")
    }
}
