package adgraph

/**
 * @author uh on 2018/07/02.
 */
class AdRepository {

    val ads = mapOf(
            1L to Ad(id = 1L, title = "cosa1", categories = listOf(Category(12, "a"), Category(42, "b"))),
            2L to Ad(id = 2L, title = "cosa2", categories = listOf(Category(13, "c"), Category(23, "d")))
    )

    fun getAll(ids: List<Long>): List<Ad> {
        return ads.toList().filter { ids.contains(it.first) }.map { it.second }
    }

    fun get(id: Long): Ad? {
        return ads[id]
    }
}
