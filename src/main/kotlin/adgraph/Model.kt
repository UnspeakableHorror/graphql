package adgraph

/**
 * @author uh on 2018/07/02.
 */

data class Ad(val id: Long,
              val title: String,
              val categories: List<Category>)

data class Category(val id: Long, val name: String)
