package adgraph

import com.github.pgutkowski.kgraphql.KGraphQL
import graph.Droid

/**
 * @author uh on 2018/07/02.
 */
class QL(private val repository: AdRepository) {
    private val schema = KGraphQL.schema {

        configure {
            useDefaultPrettyPrinter = true
        }

        query("ads") {
            resolver{ ids: List<Long> -> repository.getAll(ids) }
        }

        query("ad") {
            resolver { id: Long -> repository.get(id) }
        }

        type<Category>()
    }

    fun execute(query: String): String {
        return schema.execute(query)
    }
}
