package graph

import com.github.pgutkowski.kgraphql.KGraphQL

/**
 * @author uh on 2018/07/02.
 */
val schema = KGraphQL.schema {

    //configure method allows you customize schema behaviour
    configure {
        useDefaultPrettyPrinter = true
    }

    //create query "hero" which returns instance of Character
    query("hero") {
        resolver{ episode: Episode -> when(episode){
            Episode.NEWHOPE, Episode.JEDI -> r2d2
            Episode.EMPIRE -> luke
        }}
    }

    //create query "heroes" which returns list of luke and r2d2
    query("heroes") {
        resolver{ -> listOf(luke, r2d2) }
    }

    //kotlin classes need to be registered with "type" method
    //to be included in created schema type system
    //class Character is automatically included,
    //as it is return type of both created queries
    type<Droid>()
    type<Human>()
    enum<Episode>()
}
