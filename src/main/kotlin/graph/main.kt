package graph

import java.util.stream.Collectors

fun main(args: Array<String>) {
    val s = listOf("a", "b", "c").stream().collect(Collectors.joining(","))

    println(s)
}